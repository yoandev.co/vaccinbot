# VaccinBot

VaccinBot is a Bot for Twitter. It retrieves data in OpenData from the French government, and posts on Twitter.

## Installation

Add your Twitter API credentials in the .env

```bash
composer install
```


## Usage

Post on twitter

```bash
php bin/console bot:post
```

## Data source
[Données relatives aux personnes vaccinées contre la Covid-19](https://www.data.gouv.fr/fr/datasets/donnees-relatives-aux-personnes-vaccinees-contre-la-covid-19/)

## Author
[Yoan Bernabeu](https://gitlab.com/yoan.bernabeu)