<?php

namespace App\Service;

use DateTime;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class GetDataService
{
    private $client;
    private $format;
    private $regionNameService;

    public function __construct(HttpClientInterface $client, FormatService $format, RegionNameService $regionNameService)
    {
        $this->client = $client;
        $this->format = $format;
        $this->regionNameService = $regionNameService;
    }

    public function france(): string
    {

        $response = $this->client->request(
            'GET',
            'https://www.data.gouv.fr/fr/datasets/r/c0693f46-9980-4a80-9111-f7e6f0f2539c'
        );

        $lastData = array_key_last($response->toArray());

        $date1 = new DateTime($response->toArray()[$lastData]['jour']);
        $today = $this->format->date($date1);

        $date2 = date_modify($date1, '-1 day');
        $yesterday = $this->format->date($date2);

        $nombre = $this->format->number($response->toArray()[$lastData]['n_cum_dose1']);
        $new = $this->format->number($response->toArray()[$lastData]['n_dose1']);

        return '[FRANCE] Au ' . $today . ', il y a ' . $nombre . ' personnes de vaccinées en France avec ' . $new . ' vaccinations de plus que le ' . $yesterday . ' ! #Vaccination #COVID19';
    }

    public function random(): string
    {

        $response = $this->client->request(
            'GET',
            'https://www.data.gouv.fr/fr/datasets/r/735b0df8-51b4-4dd2-8a2d-8e46d77d60d8'
        );

        $csv = $response->getContent();
        $array = array_map("str_getcsv", explode("\n", $csv));

        $arrayRandom = [
            1,
            2,
            3,
            4,
            6,
            11,
            24,
            27,
            28,
            32,
            44,
            52,
            53,
            75,
            76,
            84,
            93,
            94,
        ];

        $array_rand = array_rand($arrayRandom, 1);
        $random = $arrayRandom[$array_rand];

        foreach ($array as $item) {
            $reg = intval(explode(';', strval($item[0]))[0]);
            if ($reg == $random) {
                $data[] = $item[0];
            }
        }

        $lastData = array_key_last($data);
        $data = $data[$lastData];

        $date = new DateTime(explode(';', strval($data))[1]);
        $nombre = $this->format->number(explode(';', strval($data))[3]);
        $region = $this->regionNameService->name($random);

        return '[' . $region . ']' . ' Au ' . $date->format('d/m/Y') . ', il y a ' . $nombre . ' personnes de vaccinées ! #Vaccination #COVID19';
    }

}