<?php

namespace App\Service;

use Abraham\TwitterOAuth\TwitterOAuth;
use DateTime;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class RegionNameService
{
    public function name(int $numReg): string
    {
        switch ($numReg) {
            case 01:
                return 'Guadeloupe';
                break;
            case 02:
                return 'Martinique';
                break;
            case 03:
                return 'Guyane';
                break;
            case 04:
                return 'La Réunion';
                break;
            case 06:
                return 'Mayotte';
                break;
            case 11:
                return 'Ile-de-France';
                break;
            case 24:
                return 'Centre-Val de Loire';
                break;
            case 27:
                return 'Bourgogne-Franche-Comté';
                break;
            case 28:
                return 'Normandie';
                break;
            case 32:
                return 'Hauts-de-France';
                break;
            case 44:
                return 'Grand Est';
                break;
            case 52:
                return 'Pays de la Loire';
                break;
            case 53:
                return 'Bretagne';
                break;
            case 75:
                return 'Nouvelle-Aquitaine';
                break;
            case 76:
                return 'Occitanie';
                break;
            case 84:
                return 'Auvergne-Rhône-Alpes';
                break;
            case 93:
                return 'Provence-Alpes-Côte d\'Azur';
                break;
            case 94:
                return 'Corse';
                break;
            default:
                return strval($numReg);
            
        }
    }
}