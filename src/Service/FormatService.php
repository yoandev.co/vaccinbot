<?php

namespace App\Service;

use Abraham\TwitterOAuth\TwitterOAuth;
use DateTime;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class FormatService
{
    public function date(DateTime $date)
    {
        return $date->format('d/m/Y');
    }

    public function number($number)
    {
        return number_format($number, 0, ',', ' ');
    }
}